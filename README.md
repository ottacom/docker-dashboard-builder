# How to use r-shiny container
Please before starting make sure that:
    a)  You have docker on your computer
    b)  You are logged into https://hub.docker.com/

---





# Preparing your code repository
From your terminal or Powershell/Dos in case you are using windows create yuor own folder with your R code into your local computer or simply clone your own repository ,for instance: 

    a) Windows: mkdir C:\myproject
    b) Linux: mkdir /myproject
    c) In case you already have your own git repo: git clone https://frpngc@bitbucket.org/genomicepidemiology/epidemobility.git


# Running r-shiny container
1. From your terminal or Powershell/Dos in case you are using windows and run this command
    
    
        
        docker login
        Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
        Username: dockerfrp
        Password:
        Login Succeeded
        
    
    Please keep in mind to run docker login only the first time, once you are logged a token will be saved on your computer, so you don need to login into the repo everytime.

2.  From your terminal or Powershell/Dos in case you are using windows and run this command:
      ```docker pull dockerfrp/r-shiny```

3.  Now you are ready to start your application into the r-shiny container.The contaier has his own R env ready and updated  
    
    ```
    docker run --name myapp --hostname myapp -v <your-code-directory>:/<your-mount-point> -d -p 80:80 dockerfrp/r-shiny   tail -f /dev/null
    ```

        where: 
        --name : is the name of your container
        --hostname: is the hostname of your container
        -v : is the Volume mountpoint where you will find yuor code
        -d : run in background
        -p : it will expose the http port 80
        tail -f /dev/null is just a temporary entrypoint to keep your container always on

    For instance in my case:

      
        docker run --name myapp --hostname myapp -v /Users/ottacom/Documents/Development/epidemobility:/epidemobility -d -p 80:80 dockerfrp/r-shiny   tail -f /dev/null
      
4.  Now you are ready to go and develop your stuff, from your terminal please type:
        
        docker ps

    If everytings is fine you should have an output like this:

        docker ps
        CONTAINER ID        IMAGE               COMMAND               CREATED              STATUS              PORTS                NAMES
        e97c7b1a2863        dockerfrp/r-shiny   "tail -f /dev/null"   About a minute ago   Up About a minute   0.0.0.0:80->80/tcp   myapp

    Remind: You can code directly into the direcory on your local computer (as usual) which it has been mounted also into the container (in my case was /Users/ottacom/Documents/Development/epidemobility)**


5.  Jumping into your container and run your app:
    From your termianl please type

        docker exec -it myapp bash
    
    And you will be redirect into the container consolle 
        root@myapp:/#
    To run your shiny application just point into your mount point ( in my case is /epidemobility ) and run your app (im my case Rscript app.r)
    Just type exit to go out

    **To see your application in action open your browser at http://localhost**

6.  Missing packages
    In case your application wouldn't start becasue you need to install some specific packages you can also use the R conslle typing for instance inside your container:
        
        R
        > install.packages("na.tools")

7. Snapshotting your container
   If you want to save what you did with your contained (for instance you  installed some extra packages) and you want to preserve your new env you can run this commannd
 
        docker commit myapp myappv1.1
    So in this way your container will preserve the env and the next time you can start from the the new version instead 

        docker run --name myapp --hostname myapp1.1 -v <your-code-directory>:/<your-mount-point> -d -p 80:80 dockerfrp/r-shiny   tail -f /dev/null
    
    **Please watch out that I'm running the container version called myapp1.1 and not myapp**

8.  How can I include my code into the container and publish my container?
     There is a Dockerfile template into this directory , but you should know how you can compile that (https://docs.docker.com/compose/gettingstarted/)
     please Download the Dockerfile and customize the file as you like then from the same directory where the Dockerfile is located please type 
        
        docker build . -t mynewcontainer

9.  Now you are ready to push the container into the internet repository , **please keep in mind that if you following this guide your container will published with the code directory on board, if you have sensitive code or data you should use a private registry**
    
        docker tag mynewcontainer <your-docker-login>/mynewcontainer
        docker push <your-docker-login>/mynewcontainer
   
     
   
